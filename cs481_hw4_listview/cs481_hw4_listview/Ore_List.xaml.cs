﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace cs481_hw4_listview
{
    public class ores
    {
        public string Name { get; set; } //data binding Name from cell list
        public string Image { get; set; }//data binding Image from cell list
        


    }
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Ore_List : ContentPage
    {
        public ObservableCollection<ores> ore_list { get; set; }   //creating a list of ores to get information and set the information for further use
        public Ore_List()
        {
            InitializeComponent();
            Call_ores();
        }


        /*
         * This function creates a list of cells that hold information about different types of Terraria pre-hardmode ores using binding context 
         * for the image and the name
         * 
         * All of the information that I used to create these pages was pooled together from my own personal in-game experience as well as from 
         * https://terraria.fandom.com/wiki/Ores
         */
        private void Call_ores()
        {
            ore_list = new ObservableCollection<ores>()
            {
                new ores()
                {
                    Name = "Copper Ore",       //Copper Ore
                    Image = "Copper.PNG",
                   
                },
                new ores()
                {
                    Name = "Tin Ore",       //Tin Ore
                    Image = "Tin.JPG",
                    
                },
                new ores()
                {
                    Name = "Iron Ore",       //Iron Ore
                    Image = "Iron.PNG",
                   
                },

                new ores()
                {
                    Name = "Lead Ore",       //Lead Ore
                    Image = "Lead.PNG",
                    
                },
                new ores()
                {
                    Name = "Silver Ore",       //Silver Ore
                    Image = "Silver.PNG",
                    
                },
                new ores()
                {
                    Name = "Tungsten Ore",       //Tungsten Ore
                    Image = "Tungsten.PNG",
                    
                },
                new ores()
                {
                    Name = "Gold Ore",       //Gold Ore
                    Image = "Gold.PNG",
                    
                },
                new ores()
                {
                    Name = "Platinum Ore",       //Platinum Ore
                    Image = "Platinum.PNG",
                   
                },
                new ores()
                {
                    Name = "Meteorite Ore",       //Meteorite Ore
                    Image = "Meteorite.PNG",
                    
                },
                new ores()
                {
                    Name = "Demonite Ore",       //Demonite Ore
                    Image = "Demonite.PNG",
                    
                },
                new ores()
                {
                    Name = "Crimtane Ore",       //Crimtane Ore
                    Image = "Crimtane.PNG",
                   
                },
                new ores()
                {
                    Name = "Obsidian Ore",       //Obsidian Ore
                    Image = "Obsidian.PNG",
                    
                },
                new ores()
                {
                    Name = "Hellstone Ore",       //Hellstone ore
                    Image = "Hellstone.PNG",
                    
                }
            };
            o_list.ItemsSource = ore_list;

        }

        //Refreshes the list upon pulling to refresh then sets "isRefreshing" back to false; Refreshing the page brings back deleted cells
        private void o_list_Refreshing(object sender, EventArgs e)
        {
            Call_ores();
            o_list.IsRefreshing = false;
        }

        /*Treats each cell as a data cell; where the user would need to hold the cell in order to access the data by clicking on the name "data"
        * 
        * After clicking and holding for a short time on the name "data" at the top next to delete, the on_click event will trigger bringing the user to the cell's page where they clicked
        */
        private void Selected_Cell(object sender, EventArgs e)
        {
            var add = ((MenuItem)sender);

            ores material = (ores)add.CommandParameter;

            if (material.Name == "Copper Ore")
            {
                Navigation.PushAsync(new Copper_Ore());
            }
            else if (material.Name == "Tin Ore")
            {
                Navigation.PushAsync(new Tin_Ore());
            }
            else if (material.Name == "Iron Ore")
            {
                Navigation.PushAsync(new Iron_Ore());
            }
            else if (material.Name == "Lead Ore")
            {
                Navigation.PushAsync(new Lead_Ore());
            }
            else if (material.Name == "Silver Ore")
            {
                Navigation.PushAsync(new Silver_Ore());
            }
            else if (material.Name == "Tungsten Ore")
            {
                Navigation.PushAsync(new Tungsten_Ore());
            }
            else if (material.Name == "Gold Ore")
            {
                Navigation.PushAsync(new Gold_Ore());
            }
            else if (material.Name == "Platinum Ore")
            {
                Navigation.PushAsync(new Platinum_Ore());
            }
            else if (material.Name == "Meteorite Ore")
            {
                Navigation.PushAsync(new Meteorite_Ore());
            }
            else if (material.Name == "Demonite Ore")
            {
                Navigation.PushAsync(new Demonite_Ore());
            }
            else if (material.Name == "Crimtane Ore")
            {
                Navigation.PushAsync(new Crimtane_Ore());
            }
            else if (material.Name == "Obsidian Ore")
            {
                Navigation.PushAsync(new Obsidian_Ore());
            }
            else if (material.Name == "Hellstone Ore")
            {
                Navigation.PushAsync(new Hellstone_Ore());

            }
        }

        /*This menuItem context can delete a cell, this site helped me form a better Menu:
         * https://stackoverflow.com/questions/5488397/c-sharp-windows-forms-menuitem-click-event-getting-the-menuitem-text
         *
         */
        private void Delete_Cell(object sender, EventArgs e)
        {
            var remove = ((MenuItem)sender);
            var trash = (ores)remove.CommandParameter;
            ore_list.Remove(trash);

        }

    }

}